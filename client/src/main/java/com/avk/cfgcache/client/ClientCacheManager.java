package com.avk.cfgcache.client;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.avk.cfgcache.NamedThreadFactory;
import com.avk.cfgcache.Transport;
import com.avk.cfgcache.TransportListener;
import com.avk.cfgcache.CfgCacheConstants;
import com.avk.cfgcache.helpers.SerializationHelper;
import com.avk.cfgcache.messages.CacheMessage;

public class ClientCacheManager {
	
	private static final Logger logger = Logger.getLogger(ClientCacheManager.class);
	
	private TransportListener cacheUpdateListener = new CacheUpdateTransportListener();
		
	private Map<String, ClientCacheImpl> clientCacheByName;
	private Transport transport;
	
	private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory("client-cache-manager-ctx-thread", true));
	
	public ClientCacheManager(Map<String,ConcurrentHashMap> caches,
							Transport transport) {
		this.transport = transport;
		this.clientCacheByName = new HashMap<String, ClientCacheImpl>();
		Set<String> names = caches.keySet();
		for (String name : names) {
			logger.info("Adding cache " + name);
			ConcurrentHashMap cache = caches.get(name); 
			clientCacheByName.put(name, new ClientCacheImpl(cache, name, -1, transport));
		}
	}
	
	public void start() {
		logger.info("Starting client cache manager...");
		transport.subscribe(CfgCacheConstants.CACHE_UPDATE_TOPIC_NAME, cacheUpdateListener);
		executor.scheduleAtFixedRate(new HeartbeatTracker(), 
										CfgCacheConstants.HEARTBEAT_PERIOD_MILLIS, 
										CfgCacheConstants.HEARTBEAT_PERIOD_MILLIS, 
										TimeUnit.MILLISECONDS);
		logger.info("Client cache manager has been srated");
	}
		
	public void stop() {
		logger.info("Stopping ClientCacheManager...");
		transport.unsubscribe(CfgCacheConstants.CACHE_UPDATE_TOPIC_NAME, cacheUpdateListener);
		executor.shutdownNow();
		logger.info("ClientCacheManager has been stopped");
	}
	
	public ClientCache getCache(String name) {
		return clientCacheByName.get(name);
	}
	
	private class HeartbeatTracker implements Runnable {

		@Override
		public void run() {
			try {
				Collection<ClientCacheImpl> caches = clientCacheByName.values();
				if(caches == null) {
					return;
				}
				for (ClientCacheImpl clientCache : caches) {
					clientCache.checkHeartbeat();
				}
			} catch (Throwable t) {
				logger.info("Error tracking heartbeats" ,t);
			}
		}
	}
	
	private class TransportEventProcessor implements Runnable {

		private byte[] data;
		
		public TransportEventProcessor(byte[] data) {
			this.data = data;
		}
		
		@Override
		public void run() {
			try {
				CacheMessage msg = (CacheMessage)SerializationHelper.deserializeFromByetArray(data);
				String cacheName = msg.getCacheName();
				ClientCacheImpl clientCache = clientCacheByName.get(cacheName);
				if(clientCache == null) {
					logger.info("received event for unknown cache: " + cacheName);
				} else {
					clientCache.processCacheMessage(msg);
				}
			} catch (Throwable t) {
				logger.info("Error processing cache message", t);
			} 	
		}
		
	}
	
	private class CacheUpdateTransportListener implements TransportListener {

		@Override
		public void onMessage(byte[] data, String topic) {
			executor.execute(new TransportEventProcessor(data));
		}		
	}
}
