package com.avk.cfgcache.client;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import com.avk.cfgcache.CfgCacheConstants;
import com.avk.cfgcache.Transport;
import com.avk.cfgcache.helpers.SerializationHelper;
import com.avk.cfgcache.messages.CacheAllDataMessage;
import com.avk.cfgcache.messages.CacheMessage;
import com.avk.cfgcache.messages.CachePutMessage;
import com.avk.cfgcache.messages.CacheFillGapMessage;
import com.avk.cfgcache.messages.CacheRemoveMessage;
import com.avk.cfgcache.messages.FillGapRequest;
import com.avk.cfgcache.messages.GetAllCacheDataRequest;
import com.avk.cfgcache.messages.CacheHeartbeatMessage;

class ClientCacheImpl<K,V> implements ClientCache<K,V>{
	
	private static final Logger logger = Logger.getLogger(ClientCacheImpl.class);
	
	private ConcurrentHashMap<K,V> cache;
	private String cacheName;
	private Transport transport;
	private long previousVersion;
	private long previousHeartbeatTimestamp = 0;
	
	ClientCacheImpl(ConcurrentHashMap<K,V> map, String name, long version, Transport transport) {
		this.cache = map;
		this.cacheName= name;
		this.previousVersion = version;
		this.transport = transport;
	}
	
	public V get(K key) {
		return cache.get(key);
	}
	
	public String getName() {
		return this.cacheName;
	}
	
	void processCacheMessage(CacheMessage msg) {
		long newVersion = msg.getVersion();
		if(previousVersion < 0) {
			loadAll();
		}
		if(newVersion - previousVersion == 1 || newVersion == previousVersion) {
			processUpdate(msg);
		} else if(newVersion - previousVersion > 1) {
			reconcileCache();
		} else {
			logger.info("Client has newer version of cache than server; skipping update from server");
		}
	}
	
	void processUpdate(CacheMessage msg) {
		if(msg instanceof CacheHeartbeatMessage) {
			logger.info("Processing heartbeat " + msg + ", cache version " + previousVersion);
			if (msg.getVersion() <= 0 || previousVersion <= 0) {
				previousHeartbeatTimestamp = System.currentTimeMillis();
				loadAll();
			} else if(previousVersion == msg.getVersion()) {
				previousHeartbeatTimestamp = System.currentTimeMillis();
			} else {
				previousHeartbeatTimestamp = System.currentTimeMillis();
				reconcileCache();
			}
		} else if (msg instanceof CachePutMessage) {
			logger.info("Processing put message " + msg + ", cache version " + previousVersion);
			CachePutMessage update = (CachePutMessage)msg;
			K[] keys = (K[])update.getKeys();
			V[] values = (V[])update.getValues();
			for(int i = 0; i < keys.length; i ++) {
				cache.put(keys[i], values[i]);
			}
			previousVersion = msg.getVersion();
			logger.info("Put has been processed, new version " + previousVersion + ", cache version " + previousVersion);
		}  else if (msg instanceof CacheRemoveMessage) {
			logger.info("Processing remove message " + msg);
			CacheRemoveMessage update = (CacheRemoveMessage)msg;
			Object[] keys = update.getKeys();
			for(int i = 0; i < keys.length; i ++) {
				cache.remove(keys[i]);
			}
			previousVersion = msg.getVersion();
			logger.info("Remove has been processed, new version " + previousVersion);
		} else {
			logger.info("Skipping unknown message " + msg + ", cache version " + previousVersion);
		}
	}
	
	void reconcileCache() {
		try {
			logger.info("Reconciling cache " + cacheName + " version " + previousVersion);
			FillGapRequest request = new FillGapRequest(previousVersion, cacheName);
			byte[] responseData = transport.request(SerializationHelper.serializeIntoBytearray(request), 
													FillGapRequest.class.getName());
			CacheFillGapMessage msg = (CacheFillGapMessage)SerializationHelper.deserializeFromByetArray(responseData);
			if(msg != null) {
				CacheMessage[] messages = msg.getMessages();
				for (CacheMessage cacheMessage : messages) {
					if(cacheMessage instanceof CachePutMessage) {
						CachePutMessage update = (CachePutMessage)cacheMessage;
						K[] keys = (K[])update.getKeys();
						V[] values = (V[])update.getValues();
						for(int i = 0; i < keys.length; i ++) {
							cache.put(keys[i], values[i]);
						}	
					} else if(cacheMessage instanceof CacheRemoveMessage) {
						CacheRemoveMessage update = (CacheRemoveMessage)cacheMessage;
						Object[] keys = update.getKeys();
						for(int i = 0; i < keys.length; i ++) {
							cache.remove(keys[i]);
						}	
					} else {
						logger.info("Unknown messages type was encountered during reconciliation message processing: " + cacheMessage);
					}
				}
				previousVersion = msg.getVersion();
			}
			else {
				logger.info("Received null trying to reconcile cache " + cacheName + ", re-loading all data");
				loadAll();
			}
		} catch (IOException e) {
			logger.info("Error reconciling client cache " + cacheName + " version " + previousVersion, e);
		} catch (ClassNotFoundException e) {
			logger.info("Error reconciling client cache " + cacheName + " version " + previousVersion, e);
		}
	}
	
	void loadAll() {
		try {
			logger.info("Load all data for cache " + cacheName);
			GetAllCacheDataRequest request = new GetAllCacheDataRequest(cacheName);
			byte[] responseData = transport.request(SerializationHelper.serializeIntoBytearray(request), 
													GetAllCacheDataRequest.class.getName());
			if(responseData == null) {
				logger.info("Can not load cache " + cacheName + " - response is null");
				return;
			}
			CacheAllDataMessage msg = (CacheAllDataMessage)SerializationHelper.deserializeFromByetArray(responseData);
			K[] keys = (K[])msg.getKeys();
			V[] values = (V[])msg.getValues();
			cache.clear();
			for(int i = 0; i < keys.length; i ++) {
				cache.put(keys[i], values[i]);
			}
			previousVersion = msg.getVersion();
		} catch (IOException e) {
			logger.info("Error loading all data for cache " + cacheName, e);
		} catch (ClassNotFoundException e) {
			logger.info("Error loading all data for cache " + cacheName, e);
		}		
	}
	
	void checkHeartbeat() {
		logger.info("checking heartbeats for " + cacheName + "; previous heartbeat: " 
						+ previousHeartbeatTimestamp + ", current time + " + System.currentTimeMillis());
		if(previousHeartbeatTimestamp + 3*CfgCacheConstants.HEARTBEAT_PERIOD_MILLIS < System.currentTimeMillis()) {
			logger.info("Lost 3 heartbeats, need to reconcile cache");
			reconcileCache();
		}
	}
	
	@Override
	public String toString() {
		return "ClientCache [cache=" + cache + ", cacheName=" + cacheName
				+ ", transport=" + transport + ", previousVersion="
				+ previousVersion + "]";
	}
}
