package com.avk.cfgcache.client;

public interface ClientCache<K,V> {
	V get(K key);
	String getName();
}
