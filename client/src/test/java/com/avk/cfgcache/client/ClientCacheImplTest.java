package com.avk.cfgcache.client;


import java.util.concurrent.ConcurrentHashMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import com.avk.cfgcache.Transport;
import com.avk.cfgcache.helpers.SerializationHelper;
import com.avk.cfgcache.messages.CacheAllDataMessage;
import com.avk.cfgcache.messages.CacheMessage;
import com.avk.cfgcache.messages.CachePutMessage;
import com.avk.cfgcache.messages.CacheFillGapMessage;
import com.avk.cfgcache.messages.CacheRemoveMessage;
import com.avk.cfgcache.messages.FillGapRequest;
import com.avk.cfgcache.messages.GetAllCacheDataRequest;
import com.avk.cfgcache.messages.CacheHeartbeatMessage;

public class ClientCacheImplTest {
	ClientCacheImpl<String, String> cache;
	Transport t;
	ConcurrentHashMap<String, String> clientTestCache;
	
	@Before
	public void setUp() throws Exception {
		t = mock(Transport.class);
		clientTestCache = new ConcurrentHashMap<String, String>();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLoadAllIfEmpty() throws Exception {
		cache = new ClientCacheImpl<String, String>(clientTestCache, "clients", -1, t);
		CacheHeartbeatMessage msg = new CacheHeartbeatMessage("clients", 1, System.currentTimeMillis());
		GetAllCacheDataRequest request = new GetAllCacheDataRequest("clients");
		CacheAllDataMessage response = new CacheAllDataMessage("clients", 1, new String[]{"c1","c2"}, new String[]{"v1", "v2"});
		when(t.request(SerializationHelper.serializeIntoBytearray(request), GetAllCacheDataRequest.class.getName()))
			.thenReturn(SerializationHelper.serializeIntoBytearray(response));
		cache.processCacheMessage(msg);
		assertTrue(cache.get("c1").equals("v1"));
		assertTrue(cache.get("c2").equals("v2"));
	}
	
	@Test
	public void testReconcileIfVersionsMismatch() throws Exception {
		cache = new ClientCacheImpl<String, String>(clientTestCache, "clients", 0, t);
		CachePutMessage msg = new CachePutMessage(2, "clients", new String[]{"c2"}, new String[]{"v2"});
		FillGapRequest request = new FillGapRequest(0, "clients");
		CacheFillGapMessage response = new CacheFillGapMessage("clients", 2, 
						new CacheMessage[] {new CachePutMessage(1, "clients", new String[]{"c1"}, new String[]{"v1"}),
											new CachePutMessage(1, "clients", new String[]{"c2"}, new String[]{"v2"})}); 
		when(t.request(SerializationHelper.serializeIntoBytearray(request), FillGapRequest.class.getName()))
			.thenReturn(SerializationHelper.serializeIntoBytearray(response));
		cache.processCacheMessage(msg);
		assertTrue(cache.get("c1").equals("v1"));
		assertTrue(cache.get("c2").equals("v2"));
	}
	
	@Test
	public void testLoadAllIfReconcilliationFailed() throws Exception {
		cache = new ClientCacheImpl<String, String>(clientTestCache, "clients", 0, t);
		CacheHeartbeatMessage msg = new CacheHeartbeatMessage("clients", 2, System.currentTimeMillis());
		
		FillGapRequest request = new FillGapRequest(0, "clients");
		when(t.request(SerializationHelper.serializeIntoBytearray(request), FillGapRequest.class.getName()))
			.thenReturn(null);
		
		GetAllCacheDataRequest request2 = new GetAllCacheDataRequest("clients");
		CacheAllDataMessage response2 = new CacheAllDataMessage("clients", 2, new String[]{"c1","c2"}, new String[]{"v1", "v2"});
		when(t.request(SerializationHelper.serializeIntoBytearray(request2), GetAllCacheDataRequest.class.getName()))
			.thenReturn(SerializationHelper.serializeIntoBytearray(response2));
		cache.processCacheMessage(msg);
		
		
		assertTrue(cache.get("c1").equals("v1"));
		assertTrue(cache.get("c2").equals("v2"));		
	}

	@Test
	public void testAddItem() throws Exception {
		cache = new ClientCacheImpl<String, String>(clientTestCache, "clients", 0, t);
		CachePutMessage msg = new CachePutMessage(1, "clients", new String[]{"c1"}, new String[]{"v1"});
		cache.processCacheMessage(msg);
		assertTrue(cache.get("c1").equals("v1"));
	}
	
	@Test
	public void testUpdateItem() throws Exception {
		clientTestCache.put("c1", "v1");
		cache = new ClientCacheImpl<String, String>(clientTestCache, "clients", 0, t);
		CachePutMessage msg = new CachePutMessage(1, "clients", new String[]{"c1"}, new String[]{"v2"});
		cache.processCacheMessage(msg);
		assertTrue(cache.get("c1").equals("v2"));
	}
	
	@Test
	public void testRemoveItem() throws Exception {
		clientTestCache.put("c1", "v1");
		cache = new ClientCacheImpl<String, String>(clientTestCache, "clients", 0, t);
		CacheRemoveMessage msg = new CacheRemoveMessage("clients", 1, new String[]{"c1"});
		cache.processCacheMessage(msg);
		assertTrue(cache.get("c1") == null);
	}
}
