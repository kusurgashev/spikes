package com.avk.cfgcache.messages;

import java.util.Arrays;

public class CacheRemoveMessage extends CacheMessage {

	private static final long serialVersionUID = 1L;
	
	private Object[] keys;
	
	public CacheRemoveMessage(String cacheName, long version, Object[] keys) {
		super(cacheName, version);
		this.keys = keys;
	}

	public Object[] getKeys() {
		return keys;
	}

	@Override
	public String toString() {
		return "CacheRemoveMessage [keys=" + Arrays.toString(keys)
				+ ", version=" + version + ", cacheName=" + cacheName + "]";
	}
}
