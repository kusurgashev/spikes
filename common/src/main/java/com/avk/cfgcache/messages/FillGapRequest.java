package com.avk.cfgcache.messages;

import java.io.Serializable;

public class FillGapRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long clientCacheVersion;
	private String cacheName;

	public FillGapRequest(long clientCacheVersion, String cacheName) {
		this.clientCacheVersion = clientCacheVersion;
		this.cacheName = cacheName;
	}

	public long getClientCacheVersion() {
		return clientCacheVersion;
	}

	public String getCacheName() {
		return cacheName;
	}

	@Override
	public String toString() {
		return "FillGapRequest [clientCacheVersion=" + clientCacheVersion
				+ ", cacheName=" + cacheName + "]";
	}
}
