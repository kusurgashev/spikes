package com.avk.cfgcache.messages;

import java.io.Serializable;
import java.util.Arrays;

public class CacheAllDataMessage extends CacheMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Object[] keys;
	private Object[] values;
	public CacheAllDataMessage(String cacheName,
							long version,  
							Object[] keys,
							Object[] values) {
		super(cacheName, version);
		this.keys = keys;
		this.values = values;
	}
	
	public Object[] getKeys() {
		return keys;
	}

	public Object[] getValues() {
		return values;
	}

	@Override
	public String toString() {
		return "CacheAllDataMessage [keys=" + Arrays.toString(keys)
				+ ", values=" + Arrays.toString(values) + ", version="
				+ version + ", cacheName=" + cacheName + "]";
	}
}
