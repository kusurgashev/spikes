package com.avk.cfgcache.messages;

import java.io.Serializable;
import java.util.Arrays;

public class CacheFillGapMessage extends CacheMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private CacheMessage[] messages;
	
	public CacheFillGapMessage(String cacheName,
										long version,
										CacheMessage[] messages) {
								
		super(cacheName, version);
		this.messages = messages;
	}

	public CacheMessage[] getMessages() {
		return messages;
	}

	@Override
	public String toString() {
		return "CacheReconciliationMessage [messages="
				+ Arrays.toString(messages) + ", version=" + version
				+ ", cacheName=" + cacheName + "]";
	}	
}
