package com.avk.cfgcache.messages;

import java.io.Serializable;

public class CacheHeartbeatMessage extends CacheMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private long timestamp;
	
	public CacheHeartbeatMessage(String cacheName, long version, long timestamp) {
		super(cacheName, version);	
		this.timestamp = timestamp;
	}

	public long getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		return "HeartbeatMessage [timestamp=" + timestamp + ", version="
				+ version + ", cacheName=" + cacheName + "]";
	}
}
