package com.avk.cfgcache.messages;

import java.io.Serializable;
import java.util.Arrays;

public class CachePutMessage extends CacheMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Object[] keys;
	private Object[] values;
	
	public CachePutMessage(long version,
								String cacheName,
								Object[] keys,
								Object[] values) {
		super(cacheName, version);
		this.keys = keys;
		this.values = values;
	}
	
	public Object[] getKeys() {
		return keys;
	}
	
	public Object[] getValues() {
		return values;
	}

	@Override
	public String toString() {
		return "CachePutMessage [keys=" + Arrays.toString(keys) + ", values="
				+ Arrays.toString(values) + ", version=" + version
				+ ", cacheName=" + cacheName + "]";
	}	
}
