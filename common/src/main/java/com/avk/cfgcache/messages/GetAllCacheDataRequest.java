package com.avk.cfgcache.messages;

import java.io.Serializable;

public class GetAllCacheDataRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cacheName;

	public GetAllCacheDataRequest(String cacheName) {
		this.cacheName = cacheName;
	}

	public String getCacheName() {
		return cacheName;
	}

	@Override
	public String toString() {
		return "GetAllCacheDataRequest [cacheName=" + cacheName + "]";
	}
}
