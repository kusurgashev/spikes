package com.avk.cfgcache.messages;

import java.io.Serializable;

public abstract class CacheMessage implements Serializable{

	private static final long serialVersionUID = 1L;
	protected long version;
	protected String cacheName;

	public CacheMessage(String cacheName,long version) {
		this.cacheName = cacheName;
		this.version = version;
	}

	public long getVersion() {
		return version;
	}

	public String getCacheName() {
		return cacheName;
	}

	@Override
	public String toString() {
		return "CacheMessage [version=" + version + ", cacheName=" + cacheName
				+ "]";
	}
}