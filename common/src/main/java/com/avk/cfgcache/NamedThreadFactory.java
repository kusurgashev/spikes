package com.avk.cfgcache;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class NamedThreadFactory implements ThreadFactory {

	final private String namePrefix; 
	final private ThreadGroup group;
	final private AtomicInteger threadNumber = new AtomicInteger(1);
	private boolean daemon; 
	
	public NamedThreadFactory(String namePrefix, boolean daemon) {
		this.namePrefix = namePrefix;
		this.daemon = daemon;
		SecurityManager s = System.getSecurityManager();
        group = (s != null)? s.getThreadGroup() :
                             Thread.currentThread().getThreadGroup();
	}
	
	@Override
	public Thread newThread(Runnable r) {
		 Thread t = new Thread(group, 
				 				r,
				 				namePrefix + threadNumber.getAndIncrement(),
				 				0);
		 t.setDaemon(daemon);
		 if (t.getPriority() != Thread.NORM_PRIORITY)
			 t.setPriority(Thread.NORM_PRIORITY);
		 return t;
	}
}
