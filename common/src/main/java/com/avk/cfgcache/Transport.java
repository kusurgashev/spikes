package com.avk.cfgcache;

public interface Transport {
	void subscribe(String topic, TransportListener listener);
	void unsubscribe(String topic, TransportListener listener);
	void publish(byte[] data, String topic);
	byte[] request(byte[] request, String type);
	void registerRequestService(String requestType, TransportRequestSerivce service);
	void unregisterRequestService(String requestType, TransportRequestSerivce service);
}
