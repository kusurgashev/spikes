package com.avk.cfgcache;

public interface CfgCacheConstants {

	public static final String CACHE_UPDATE_TOPIC_NAME = "CACHE_UPDATE";
	
	public static final int REQUEST_EXECUTION_TIMEOUT_MILLIS = 30000;
	
	public static final int HEARTBEAT_PERIOD_MILLIS = 30000;

}