package com.avk.cfgcache.helpers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public final class SerializationHelper {
	public static byte[] serializeIntoBytearray(Serializable cmd) throws IOException {
		if(cmd == null) {
			return null;
		}
		ByteArrayOutputStream bos = null;
		ObjectOutput out = null;
		try {
			bos = new ByteArrayOutputStream();
			out = new ObjectOutputStream(bos);   
			out.writeObject(cmd);
			return bos.toByteArray();
		} finally {
			if(out != null) {
				out.close();
			}
			if(bos != null) {
				bos.close();
			}
		}
	}
	
	public static Object deserializeFromByetArray(byte[] data) throws IOException, ClassNotFoundException {
		if(data == null) {
			return null;
		}
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
		Object result = ois.readObject();
		return result;
	}

}
