package com.avk.cfgcache;

public interface TransportListener {
	void onMessage(byte[] data, String topic);
}
