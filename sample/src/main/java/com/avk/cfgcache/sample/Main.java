package com.avk.cfgcache.sample;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.avk.cfgcache.Transport;
import com.avk.cfgcache.client.ClientCacheManager;
import com.avk.cfgcache.server.InMemoryReplayableStorage;
import com.avk.cfgcache.server.ReplayableStorage;
import com.avk.cfgcache.server.ServerCacheManager;

public class Main {
	
	private static final Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args) {
		Transport t = new SampleTransport();
		ReplayableStorage s = new InMemoryReplayableStorage(10);
		Map<String, Map> serverCaches = new HashMap<String, Map>();
		HashMap serverTestCache = new HashMap();
		serverTestCache.put("client1", "ladder1");
		serverCaches.put("clients", serverTestCache);
		
		ServerCacheManager server = new ServerCacheManager(serverCaches, s, t);
		server.start();
		

		Map<String, ConcurrentHashMap> clientCaches = new HashMap<String, ConcurrentHashMap>();
		ConcurrentHashMap clientTestCache = new ConcurrentHashMap();
		clientCaches.put("clients", clientTestCache);

		ClientCacheManager client = new ClientCacheManager(clientCaches, t);
		client.start();

		
		logger.info(client.getCache("clients"));
		server.put("clients", "client2", "ladder2");
		server.put("clients", "client3", "ladder3");
		server.put("clients", "client4", "ladder4");
		server.put("clients", "client5", "ladder5");
		server.put("clients", "client6", "ladder6");
		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
		}
		logger.info("after put: " + client.getCache("clients"));
		server.remove("clients", "client2");
		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
		}
		logger.info("after remove: " + client.getCache("clients"));
		server.stop();
		client.stop();
	}
}
