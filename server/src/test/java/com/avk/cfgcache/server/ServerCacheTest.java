package com.avk.cfgcache.server;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ServerCacheTest {

	ServerCache<String, String> cache;
	HashMap<String, String> map;
	
	@Before
	public void setUp() throws Exception {
		map = new HashMap<String, String>();
		cache = new ServerCache<String, String>(map, 0);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPut() throws InterruptedException, ExecutionException, TimeoutException {
		cache.put("c2", "v2");
		assertTrue(cache.getVersion() == 1);
		assertTrue(map.get("c2").equals("v2"));
	}

	@Test
	public void testRemove() throws InterruptedException, ExecutionException, TimeoutException {
		map.put("c1", "v1");
		cache.remove("c3");
		assertTrue(cache.getVersion() == 0);
		cache.remove("c1");
		assertTrue(cache.getVersion() == 1);
		assertTrue(map.get("c1") == null);
	}

}
