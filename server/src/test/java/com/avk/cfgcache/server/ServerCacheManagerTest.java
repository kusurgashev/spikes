package com.avk.cfgcache.server;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.avk.cfgcache.Transport;
import com.avk.cfgcache.messages.CacheAllDataMessage;
import com.avk.cfgcache.messages.CachePutMessage;
import com.avk.cfgcache.messages.CacheFillGapMessage;

public class ServerCacheManagerTest {

	@Mock
	Transport t;
	ServerCacheManager server;
	
	@Before
	public void setUp() throws Exception {
		t = new TransportMock();
		ReplayableStorage s = new InMemoryReplayableStorage(10);
		Map<String, Map> serverCaches = new HashMap<String, Map>();
		HashMap<String,String> serverTestCache = new HashMap<String,String>();
		serverTestCache.put("c1", "v1");
		serverCaches.put("clients", serverTestCache);		
		server = new ServerCacheManager(serverCaches, s, t);
		server.start();
	}
	
	@After
	public void tearDown() throws Exception {
		server.stop();
	}

	@Test
	public void testPut() throws InterruptedException, ExecutionException, TimeoutException {
		server.put("clients", "c2", "v2");
		assertTrue(server.getCacheVersion("clients") == 1);
		CacheAllDataMessage msg = server.getAllCacheData("clients");
		assertTrue(Arrays.binarySearch(msg.getKeys(), "c2") != -1);
		assertTrue(Arrays.binarySearch(msg.getValues(), "v2") != -1);
	}

	@Test
	public void testRemove() throws InterruptedException, ExecutionException, TimeoutException {
		server.remove("clients", "c3");
		assertTrue(server.getCacheVersion("clients") == 0);
		server.remove("clients", "c1");
		assertTrue(server.getCacheVersion("clients") == 1);
		CacheAllDataMessage msg = server.getAllCacheData("clients");
		assertTrue(Arrays.binarySearch(msg.getKeys(), "c1") == -1);
		assertTrue(Arrays.binarySearch(msg.getValues(), "v1") == -1);
	}
	
	@Test
	public void testGetAllCacheData() throws InterruptedException, ExecutionException, TimeoutException {
		CacheAllDataMessage msg = server.getAllCacheData("clients");
		assertTrue(msg.getCacheName().equals("clients"));
		assertTrue(msg.getVersion() == 0);
		assertTrue(Arrays.binarySearch(msg.getKeys(), "c1") != -1);
		assertTrue(Arrays.binarySearch(msg.getValues(), "v1") != -1);
	}
	
	@Test
	public void testGetFillGapMessage() throws InterruptedException, ExecutionException, TimeoutException {
		server.put("clients", "c2", "v2");
		CacheFillGapMessage m = server.getReconciliationMessage("clients", 0);
		assertTrue(m.getCacheName().equals("clients"));
		assertTrue(m.getVersion() == 1l);
		assertTrue(((CachePutMessage)m.getMessages()[0]).getKeys()[0].equals("c2"));
		assertTrue(((CachePutMessage)m.getMessages()[0]).getValues()[0].equals("v2"));		
	}
}
