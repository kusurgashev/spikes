package com.avk.cfgcache.server;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.avk.cfgcache.messages.CacheMessage;
import com.avk.cfgcache.messages.CachePutMessage;

public class InMemoryReplayableStorageTest {

	InMemoryReplayableStorage s;
	CacheMessage[] messages; 
	
	@Before
	public void setUp() throws Exception {
		s = new InMemoryReplayableStorage(5);
		messages = new CacheMessage[10];
		for(int i = 0; i < 10; i++) {
			messages[i] = createCacheMessage(i);
		}
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReplayMessages() {
		s.storeCacheMessage(messages[0]);
		s.storeCacheMessage(messages[1]);
		s.storeCacheMessage(messages[2]);
		s.storeCacheMessage(messages[3]);
		CacheMessage[] r1 = s.getMessages("clients", 1, 3);
		assertTrue(r1[0].equals(messages[2]));
		assertTrue(r1[1].equals(messages[3]));
	}
	
	@Test
	public void testCannotReplayMessages() {
		s.storeCacheMessage(messages[0]);
		s.storeCacheMessage(messages[1]);
		s.storeCacheMessage(messages[2]);
		s.storeCacheMessage(messages[3]);
		s.storeCacheMessage(messages[4]);
		s.storeCacheMessage(messages[5]);
		s.storeCacheMessage(messages[6]);
		assertTrue(s.getMessages("clients", 0, 2) == null);
	}
	
	CacheMessage createCacheMessage(long version) {
		return new CachePutMessage(version, "clients", new Object[]{"c" + version}, new Object[]{"v" + version});
	}
}
