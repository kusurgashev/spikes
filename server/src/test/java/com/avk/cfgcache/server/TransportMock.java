package com.avk.cfgcache.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.log4j.Logger;

import com.avk.cfgcache.Transport;
import com.avk.cfgcache.TransportListener;
import com.avk.cfgcache.TransportRequestSerivce;

public class TransportMock implements Transport {

	private static final Logger logger = Logger.getLogger(TransportMock.class);
	
	private HashMap<String, ArrayList<TransportListener>> listenersByTopic = new HashMap<String, ArrayList<TransportListener>>();
	private HashMap<String, TransportRequestSerivce> serviceByType = new HashMap<String, TransportRequestSerivce>();
	
	private ReentrantReadWriteLock listenersLock = new ReentrantReadWriteLock();
	private ReentrantReadWriteLock servicesLock = new ReentrantReadWriteLock();
	
	@Override
	public void subscribe(String topic, TransportListener listener) {
		logger.info("Subscribing topic " + topic);
		listenersLock.writeLock().lock();
		try {
			ArrayList<TransportListener> listeners = listenersByTopic.get(topic);
			if(listeners == null) {
				listeners = new ArrayList<TransportListener>();
				listenersByTopic.put(topic, listeners);
			}
			listeners.add(listener);
		} finally {
			listenersLock.writeLock().unlock();
		}
		logger.info("Topic " + topic + " has been subscribed");
	}
	
	@Override
	public void unsubscribe(String topic, TransportListener listener) {
		logger.info("Unsubscribing listener " + listener + " from topic " + topic);
		listenersLock.writeLock().lock();
		try {
			ArrayList<TransportListener> listeners = listenersByTopic.get(topic);
			if(listeners == null) {
				logger.info("Nothing to unsubscribe - ni listeners setup");
			} else {
				if(listeners.remove(listener)) {
					logger.info("listener " + listener + " has been unsubscribed from topic " + topic);
				} else {
					logger.info("listener " + listener + " was not found - failed to unsubscribe it from topic " + topic);
				}
			}
			
		} finally {
			listenersLock.writeLock().unlock();
		}
	}
	
	@Override
	public void publish(byte[] data, String topic) {
		logger.info("Publishing data on topic " + topic);
		listenersLock.readLock().lock();
		try {
			ArrayList<TransportListener> listeners = listenersByTopic.get(topic);
			if(listeners == null) {
				logger.info("No listeners were registered for topic " + topic);
			} else {
				for (TransportListener transportListener : listeners) {
					logger.info("Publishing data on topic " + topic + " for listener " + transportListener);
					transportListener.onMessage(data, topic);
				}
			}
		} finally {
			listenersLock.readLock().unlock();
		}
	}
	
	@Override
	public byte[] request(byte[] request, String type) {
		servicesLock.readLock().lock();
		try {
			TransportRequestSerivce service = serviceByType.get(type);
			if(service == null) {
				logger.info("Request service of type " + type + " is not registered");
				return null;
			} else {
				return service.onRequest(request, type);
			}
		} finally {
			servicesLock.readLock().unlock();
		}
	}
	
	@Override
	public void registerRequestService(String requestType,
										TransportRequestSerivce service) {
		logger.info("Registering service for " + requestType);
		servicesLock.writeLock().lock();
		try {
			if(serviceByType.put(requestType, service) != null) {
				logger.info("replaced request service of type " + requestType);
			} else {
				logger.info("registered request service of type " + requestType);
			}
		} finally {
			servicesLock.writeLock().unlock();
		}
		logger.info("Service for " + requestType + " has been registered");
	}
	
	@Override
	public void unregisterRequestService(String requestType,
											TransportRequestSerivce service) {
		servicesLock.writeLock().lock();
		try {
			if(serviceByType.remove(requestType) == null) {
				logger.info("Nothing to unregister - service of type " + requestType + " is already not registered");
			}
		} finally {
			servicesLock.writeLock().unlock();
		}
	}
			

}
