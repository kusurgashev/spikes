package com.avk.cfgcache.server;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.log4j.Logger;

import com.avk.cfgcache.messages.CacheMessage;

public class InMemoryReplayableStorage implements ReplayableStorage {

	private static final Logger logger = Logger.getLogger(InMemoryReplayableStorage.class); 
	
	private static final int DEFAULT_SIZE = 200;
	
	private HashMap<String, LinkedList<CacheMessage>> messagesByCacheName;
	
	private int size;
	
	private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	
	public InMemoryReplayableStorage(){
		this(DEFAULT_SIZE);
	}
	
	public InMemoryReplayableStorage(int size) {
		this.size = size;
		this.messagesByCacheName = new HashMap<String, LinkedList<CacheMessage>>();
	}
	
	@Override
	public void storeCacheMessage(CacheMessage msg) {
		lock.writeLock().lock();
		try {
			LinkedList<CacheMessage> list = messagesByCacheName.get(msg.getCacheName());
			if(list == null) {
				list = new LinkedList<CacheMessage>();
				messagesByCacheName.put(msg.getCacheName(), list);
			}
			list.addLast(msg);
			if(list.size() > size) {
				list.removeFirst();
			}
		} finally {
			lock.writeLock().unlock();
		}
	}

	@Override
	public CacheMessage[] getMessages(String cacheName, long startVersion, long endVersion) {
		logger.info("Retreiving messages for cache " + cacheName + " startVersion " + " endVersion " + endVersion);
		lock.readLock().lock();
		try {
			if(endVersion <= startVersion) {
				throw new IllegalArgumentException("startVersion (" + startVersion + ") >= endVersion ("+ endVersion +")");
			}
			LinkedList<CacheMessage> list = messagesByCacheName.get(cacheName);
			if(list == null) {
				throw new IllegalStateException("Cache not found: " + cacheName);
			}	
			CacheMessage[] result = null;
			int i = 0;
			for (CacheMessage cacheMessage : list) {
				if(cacheMessage.getVersion() == (startVersion+1) 
						&& result == null) {
					result = new CacheMessage[(int)(endVersion - startVersion)];
					result[0] = cacheMessage;
					if(endVersion == cacheMessage.getVersion()) {
						return result;
					}
					continue;
				}
				if(result != null && i < endVersion - startVersion) {
					result[++i] = cacheMessage;
				}
				if(endVersion == cacheMessage.getVersion()) {
					return result;
				}
			}
			return null;
		} finally {
			lock.readLock().unlock();
		}
	}
}
