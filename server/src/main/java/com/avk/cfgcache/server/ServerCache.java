package com.avk.cfgcache.server;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class ServerCache<K, V> {
	private long  version;
	private Map<K, V> map;
	private ReadWriteLock lock = new ReentrantReadWriteLock();
	
	public ServerCache(Map<K, V> map, long version) {
		this.map = map;
		this.version = version;
	}
	
	public long getVersion() {
		return version;
	}
	
	public long put(K key, V value) {
		lock.writeLock().lock();
		try {
			V oldValue = map.put(key, value);
			if(oldValue == null || !oldValue.equals(value)) {
				version += 1;
				return version;
			}
			return -1L;
		} finally {
			lock.writeLock().unlock();
		}
	}
	
	public long remove(K key) {
		lock.writeLock().lock();
		try {
			if(map.remove(key) != null) {
				version += 1;
				return version;
			}
			return -1L;
		} finally {
			lock.writeLock().unlock();
		}
	}
	
	public KeyValues getEntries() {
		lock.readLock().lock();
		try {
			int size = map.size();
			KeyValues result = new KeyValues(size, version);
			Set<K> keys = map.keySet();
			int index = 0;
			for (Object key : keys) {
				result.keys[index] = key;
				result.values[index] = map.get(key);
				index += 1;		
			}
			return result;
		} finally {
			lock.readLock().unlock();
		}
	}
		
	@Override
	public String toString() {
		return "ServerCache [version=" + version + ", map=" + map + "]";
	}

	public class KeyValues {
		int size;
		long version;
		Object[] keys;
		Object[] values;
		
		public KeyValues(int size, long version) {
			this.keys = new Object[size];
			this.values = new Object[size];
		}
	}	
}
