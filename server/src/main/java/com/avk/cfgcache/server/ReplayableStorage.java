package com.avk.cfgcache.server;

import com.avk.cfgcache.messages.CacheMessage;

public interface ReplayableStorage {
	void storeCacheMessage(CacheMessage msg);
	CacheMessage[] getMessages(String cacheName, long startVersion, long endVersion);
}
