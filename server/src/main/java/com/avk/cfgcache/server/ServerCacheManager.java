package com.avk.cfgcache.server;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.log4j.Logger;
import com.avk.cfgcache.NamedThreadFactory;
import com.avk.cfgcache.Transport;
import com.avk.cfgcache.TransportRequestSerivce;
import com.avk.cfgcache.CfgCacheConstants;
import com.avk.cfgcache.helpers.SerializationHelper;
import com.avk.cfgcache.messages.CacheMessage;
import com.avk.cfgcache.messages.CachePutMessage;
import com.avk.cfgcache.messages.CacheFillGapMessage;
import com.avk.cfgcache.messages.CacheRemoveMessage;
import com.avk.cfgcache.messages.FillGapRequest;
import com.avk.cfgcache.messages.GetAllCacheDataRequest;
import com.avk.cfgcache.messages.CacheAllDataMessage;
import com.avk.cfgcache.messages.CacheHeartbeatMessage;
import com.avk.cfgcache.server.ServerCache.KeyValues;

public class ServerCacheManager {
	
	private static final Logger logger = Logger.getLogger(ServerCacheManager.class);
	
	private Map<String, ServerCache> cacheByName = new HashMap<String, ServerCache>();
	private ReplayableStorage storage;
	private Transport transport;
	private GetAllCacheDataRequestService getAllRequestService = new GetAllCacheDataRequestService();
	private FillGapRequestService fillGapRequestService = new FillGapRequestService();	
	private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory("server-cache-manager-ctx-thread", true));
		
	public ServerCacheManager(Map<String,Map> caches, 
							ReplayableStorage storage,
							Transport transport) {
		this.storage = storage;
		this.transport = transport;
		Set<String> keys = caches.keySet();
		for (String key : keys) {
			logger.info("registering cache on server: " + key);
			this.cacheByName.put(key, new ServerCache(caches.get(key), 0));	
		}
	}
	
	public void start() {
		logger.info("Starting ServerCacheManager...");
		transport.registerRequestService(GetAllCacheDataRequest.class.getName(), getAllRequestService);
		transport.registerRequestService(FillGapRequest.class.getName(), fillGapRequestService);
		executor.scheduleAtFixedRate(new Heartbeater(), 0, CfgCacheConstants.HEARTBEAT_PERIOD_MILLIS, TimeUnit.MILLISECONDS);
		logger.info("ServerCacheManager has been started");
	}
	
	public void stop() {
		logger.info("Stopping ServerCacheManager...");
		transport.unregisterRequestService(GetAllCacheDataRequest.class.getName(), getAllRequestService);
		transport.unregisterRequestService(FillGapRequest.class.getName(), fillGapRequestService);
		executor.shutdownNow();
		logger.info("ServerCacheManager has been stopped");
	}
	
	public void put(String cacheName, Object key, Object value) {
		if(cacheName == null) {
			throw new IllegalArgumentException("cacheName == null");
		}
		if(key == null) {
			throw new IllegalArgumentException("key == null");
		}		
		Put put = new Put(cacheName, key, value);
		executor.execute(put);
	}

	public void remove(String cacheName, Object key) {
		if(cacheName == null) {
			throw new IllegalArgumentException("cacheName == null");
		}
		if(key == null) {
			throw new IllegalArgumentException("key == null");
		}
		Remove remove = new Remove(cacheName, key);
		executor.execute(remove);
	}
	
	public long getCacheVersion(final String cacheName) throws InterruptedException, ExecutionException, TimeoutException {
		if(cacheName == null) {
			throw new IllegalArgumentException("cacheName == null");
		}		
		Future<Long> getVersion = executor.submit(new GetVersion(cacheName));
		return getVersion.get(CfgCacheConstants.REQUEST_EXECUTION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
	}
	
	public CacheAllDataMessage getAllCacheData(String cacheName) throws InterruptedException, ExecutionException, TimeoutException {
		if(cacheName == null) {
			throw new IllegalArgumentException("cacheName == null");
		}		
		GetAllCacheData getData = new GetAllCacheData(cacheName);
		Future<CacheAllDataMessage> f= (Future<CacheAllDataMessage>) executor.submit(getData);
		return f.get(CfgCacheConstants.REQUEST_EXECUTION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
	}
	
	public CacheFillGapMessage getReconciliationMessage(String cacheName, long clientVersion) throws InterruptedException, ExecutionException, TimeoutException {
		if(clientVersion < 0) {
			throw new IllegalArgumentException("Client version < 0. Client cache not loaded");
		}
		if(cacheName == null) {
			throw new IllegalArgumentException("cacheName == null");
		}
		FillGap fillGap = new FillGap(cacheName, clientVersion);
		Future<CacheFillGapMessage> result = executor.submit(fillGap);
		return result.get(CfgCacheConstants.REQUEST_EXECUTION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);		
	}
	
	private void sendAndStoreCacheUpdate(CacheMessage msg) throws IOException {
		byte data[] = SerializationHelper.serializeIntoBytearray(msg);					
		transport.publish(data, CfgCacheConstants.CACHE_UPDATE_TOPIC_NAME);
		storage.storeCacheMessage(msg);
	}

	private void sendCacheUpdate(CacheMessage msg) throws IOException {
		byte data[] = SerializationHelper.serializeIntoBytearray(msg);					
		transport.publish(data, CfgCacheConstants.CACHE_UPDATE_TOPIC_NAME);
	}
	
	private class GetAllCacheDataRequestService implements TransportRequestSerivce {

		@Override
		public byte[] onRequest(byte[] request, String requestType) {
			String cacheName = null;
			try {
				GetAllCacheDataRequest cmd = (GetAllCacheDataRequest) SerializationHelper.deserializeFromByetArray(request);
				cacheName = cmd.getCacheName();
				return SerializationHelper.serializeIntoBytearray(getAllCacheData(cacheName));					
			} catch (IOException e) {
				logger.info("Error processing load all data request fpr cache " + cacheName, e);
			} catch (ClassNotFoundException e) {
				logger.info("Error processing load all data request fpr cache " + cacheName, e);
			} catch (InterruptedException e) {
				logger.info("Error processing load all data request fpr cache " + cacheName, e);
			} catch (ExecutionException e) {
				logger.info("Error processing load all data request fpr cache " + cacheName, e);
			} catch (TimeoutException e) {
				logger.info("Error processing load all data request fpr cache " + cacheName, e);
			} 
			return null;
		}		
	}

	private class FillGapRequestService implements TransportRequestSerivce {

		@Override
		public byte[] onRequest(byte[] request, String requestType) {
			String cacheName = null;
			long clientVersion = Long.MIN_VALUE;
			try {
				FillGapRequest cmd = (FillGapRequest) SerializationHelper.deserializeFromByetArray(request);
				cacheName = cmd.getCacheName();
				clientVersion = cmd.getClientCacheVersion();				
				return SerializationHelper.serializeIntoBytearray(getReconciliationMessage(cacheName, clientVersion));					
			} catch (IOException e) {
				logger.info("Error truying to fill gap in cache " + cacheName + ", version " + clientVersion, e);
			} catch (ClassNotFoundException e) {
				logger.info("Error truying to fill gap in cache " + cacheName + ", version " + clientVersion, e);
			} catch (InterruptedException e) {
				logger.info("Error truying to fill gap in cache " + cacheName + ", version " + clientVersion, e);
			} catch (ExecutionException e) {
				logger.info("Error truying to fill gap in cache " + cacheName + ", version " + clientVersion, e);
			} catch (TimeoutException e) {
				logger.info("Error truying to fill gap in cache " + cacheName + ", version " + clientVersion, e);
			}
			return null;
		}		
	}
	
	private class GetVersion implements Callable<Long> {

		private String cacheName;
		
		public GetVersion(String cacheName) {
			this.cacheName = cacheName;
		}

		@Override
		public Long call() throws Exception {
			ServerCache c = cacheByName.get(cacheName);
			if(c == null) {
				throw new IllegalStateException("Cache not found: " + cacheName);
			}
			return c.getVersion();
		}
	}
	
	private class FillGap implements Callable<CacheFillGapMessage> {

		private String cacheName;
		private long clientVersion;
		
		public FillGap(String cacheName, long clientVersion) {
			this.cacheName = cacheName;
			this.clientVersion = clientVersion;
		}


		@Override
		public CacheFillGapMessage call() throws Exception {
			logger.info("Trying to fill gap in cache " + cacheName + ", version " + clientVersion);
			ServerCache cache = cacheByName.get(cacheName);
			if(cache == null) {
				return null;
			}
			long newVersion = cache.getVersion();
			logger.info("Server cache version: " + newVersion);
			CacheMessage[] messages = storage.getMessages(cacheName, clientVersion, newVersion);
			if(messages == null) {
				logger.info("Can not find messages in replayable store");
				return null;
			}
			return new CacheFillGapMessage(cacheName, newVersion, messages);	
		}		
	}
		
	private class GetAllCacheData implements Callable<CacheAllDataMessage> {
		private String cacheName;
		
		public GetAllCacheData(String cacheName) {
			this.cacheName = cacheName;
		}

		@Override
		public CacheAllDataMessage call() throws Exception {
			try {
				logger.info("Trying to load al data for cache " + cacheName);
				ServerCache cache = cacheByName.get(cacheName);
				if(cache == null) {
					return null;
				}
				KeyValues entries = cache.getEntries();
				return new CacheAllDataMessage(cacheName, 
												entries.version, 
												entries.keys, 
												entries.values);	
			} catch (Throwable t) {
				t.printStackTrace();
			}
			return null;
		}
	}
	
	private class Put implements Runnable {

		private String cacheName;
		private Object key;
		private Object value;
		
		public Put(String cacheName, Object key, Object value) {
			this.cacheName = cacheName;
			this.key = key;
			this.value = value;
		}

		@Override
		public void run() {
			try {
				logger.info("Putting data to cache:" + cacheName + ":"  + key + " -> " + value);
				ServerCache cache = cacheByName.get(cacheName);
				if(cache == null) {
					logger.info("Attempt to put data into unknown cache " + cacheName);
				}
				long newVersion = cache.put(key, value);
				if(newVersion > 0) {
					try {
						CachePutMessage msg = new CachePutMessage(newVersion, 
																		cacheName,
																		new Serializable[]{(Serializable)key}, 
																		new Serializable[]{(Serializable)value});
	
						sendAndStoreCacheUpdate(msg);
					} catch (IOException e) {
						
					}
				} else {
					logger.info("Cache version has not been updated by put operation" + cacheName);
				}
			} catch (Throwable t) {
				logger.info("Error putting data into server cache " + cacheName, t);
			}
		}		
	}
	
	private class Remove implements Runnable {

		private String cacheName;
		private Object key;
		
		public Remove(String cacheName, Object key) {
			this.cacheName = cacheName;
			this.key = key;
		}

		@Override
		public void run() {
			ServerCache cache = cacheByName.get(cacheName);
			if(cache == null) {
				logger.info("Attempt to put data into unknown cache " + cacheName);
			}
			long newVersion = cache.remove(key);
			if(newVersion > 0) {
				try {
					CacheRemoveMessage msg = new CacheRemoveMessage(cacheName,
																	newVersion,
																	new Serializable[]{(Serializable)key});

					sendAndStoreCacheUpdate(msg);
				} catch (IOException e) {
					logger.info("Error removing data from cacahe " + cacheName);
				}
			} else {
				logger.info("Cache version has not been updated by remove operation " + cacheName);
			}
		}		
	}
	
	private class Heartbeater implements Runnable {
		
		@Override
		public void run() {
			try {
				Set<Map.Entry<String, ServerCache>> entries = cacheByName.entrySet();
				if(entries != null) {
					for (Map.Entry<String, ServerCache> entry : entries) {
						CacheHeartbeatMessage heartbeat = new CacheHeartbeatMessage(entry.getKey(), 
																			entry.getValue().getVersion(), 
																			System.currentTimeMillis());
						
						try {
							logger.info("Sending heartbeat " + heartbeat);
							sendCacheUpdate(heartbeat);
						} catch (IOException e) {
							logger.info("Could not send heartbeat for cache " + entry.getKey());
						}
					}
				} else {
					logger.info("No caches registered");
				}
				
			} catch (Throwable t) {
				logger.info("Error sending heartbeat", t);
			}
		} 
	}	
}
